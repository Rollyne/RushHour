namespace RushHour.Data
{
    using System.Data.Entity;
    using Models.Entities;

    public class RushHourContext : DbContext
    {
        // Your context has been configured to use a 'RushHourContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'RushHour.Data.RushHourContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'RushHourContext' 
        // connection string in the application configuration file.
        public RushHourContext()
            : base("RushHour")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RushHourContext>());
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Appointment> Appointments { get; set; }

        public DbSet<Activity> Activities { get; set; }
    }
}