﻿namespace RushHour.Data.Repositories.Tools
{
    using System.Collections.Generic;

    public class Pager<TEntity> : IPager<TEntity>
    {
        public ICollection<TEntity> Model { get; set; }
        public int ItemsAvaliable { get; set; }
    }
}
