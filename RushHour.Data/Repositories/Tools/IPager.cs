﻿namespace RushHour.Data.Repositories.Tools
{
    using System.Collections.Generic;

    public interface IPager<TEntity>
    {
        int ItemsAvaliable { get; set; }
        ICollection<TEntity> Model { get; set; }
    }
}
