﻿namespace RushHour.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Linq.Expressions;
    using AutoMapper.QueryableExtensions;
    using Models.Entities;
    using Tools;

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IIdentificatable
    {
        protected DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }

        public void AddOrUpdate(TEntity item)
        {
            Context.Set<TEntity>().AddOrUpdate(item);
        }

        public void Delete(TEntity item)
        {
            Context.Set<TEntity>().Remove(item);
        }

        private IQueryable<TEntity> _getAllWhere(Expression<Func<TEntity, bool>> @where)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            query = where != null ? query.Where(where) : query;
            return query;
        }

        public ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> @where = null)
        {
            IQueryable<TEntity> query = _getAllWhere(where);

            return query.ToList();
        }

        public TEntity FirstOrDefault(Func<TEntity, bool> @where = null)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return where == null ? query.FirstOrDefault() : query.FirstOrDefault(where);
        }

        public int Count()
        {
            return Context.Set<TEntity>().Count();
        }

        public bool Any(Expression<Func<TEntity, bool>> any)
        {
            return Context.Set<TEntity>().Any(any);
        }

        public IPager<TEntity> GetAllPaged(int page, int itemsPerPage, Expression<Func<TEntity, bool>> @where = null)
        {
            IQueryable<TEntity> query = _getAllWhere(where);

            var result = new Pager<TEntity>()
            {
                ItemsAvaliable = query.Count()
            };
            if (page > 0 && itemsPerPage > 0)
            {
                query = query.OrderBy(i => i.Id).Skip(itemsPerPage * (page - 1)).Take(itemsPerPage);
            }

            result.Model = query.ToList();

            return result;
        }

        public IPager<TResult> GetAllPaged<TResult>(int page, int itemsPerPage, Expression<Func<TEntity, bool>> @where = null)
        {
            IQueryable<TEntity> query = _getAllWhere(where);

            var result = new Pager<TResult>()
            {
                ItemsAvaliable = query.Count()
            };
            if (page > 0 && itemsPerPage > 0)
            {
                query = query.OrderBy(i => i.Id).Skip(itemsPerPage * (page - 1)).Take(itemsPerPage);
            }

            result.Model = query.ProjectTo<TResult>().ToList();

            return result;
        }

        public ICollection<TResult> GetAll<TResult>(Expression<Func<TEntity, bool>> @where = null)
        {
            IQueryable<TEntity> query = _getAllWhere(where);
            return query.ProjectTo<TResult>().ToList();
        }

        public TResult FirstOrDefault<TResult>(Expression<Func<TEntity, bool>> @where = null)
        {
            var query = Context.Set<TEntity>();
            return where != null ? query.Where(where).ProjectTo<TResult>().FirstOrDefault() : query.ProjectTo<TResult>().FirstOrDefault();
        }

        public void Add(TEntity item)
        {
            Context.Set<TEntity>().Add(item);
        }
    }
}
