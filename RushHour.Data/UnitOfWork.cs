﻿namespace RushHour.Data
{
    using System.Data.Entity;
    using Models.Entities;
    using Repositories;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public UnitOfWork()
        {
            _context = new RushHourContext();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : class, IIdentificatable
            => new Repository<TEntity>(_context);

        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
