﻿namespace RushHour.Data
{
    using Repositories;
    using Models.Entities;

    using System;
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IIdentificatable;
        void Save();
    }
}