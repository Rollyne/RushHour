﻿namespace RushHour.Web.Controllers
{
    using Data;
    using Generic;
    using Models.Binding.Appointment;
    using Models.Entities;
    using Models.View.Appointment;
    using Services.Generic;
    public class AppointmentsController : CrudController<CrudService<UnitOfWork, Appointment, AppointmentIndexViewModel, AppointmentBindingModel, AppointmentDetailsViewModel, AppointmentFilterBindingModel>, AppointmentIndexViewModel, AppointmentBindingModel, AppointmentDetailsViewModel, AppointmentFilterBindingModel>
    {
    }
}