﻿namespace RushHour.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public abstract class BaseController : Controller
    {
        public void AddMessage(string message)
        {
            if (!TempData.ContainsKey("Messages"))
            {
                TempData.Add("Messages", new List<string>() { message });
            }
            else
            {
                var messages = TempData["Messages"] as List<string>;
                messages?.Add(message);
            }
        }

        public void AddError(string error)
        {
            if (!TempData.ContainsKey("Errors"))
            {
                TempData.Add("Errors", new List<string>() { error });
            }
            else
            {
                var errors = TempData["Errors"] as List<string>;
                errors?.Add(error);
            }

        }
    }
}