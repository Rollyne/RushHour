﻿

namespace RushHour.Web.Controllers
{
    using Data;
    using Generic;
    using Models.Binding.User;
    using Models.Entities;
    using Models.View.User;
    using Services.Generic;

    public class UsersController : CrudController<CrudService<UnitOfWork,User,UserIndexViewModel,UserBindingModel,UserDetailsViewModel,UserFilterBindingModel>, UserIndexViewModel, UserBindingModel, UserDetailsViewModel, UserFilterBindingModel>
    {
    }
}