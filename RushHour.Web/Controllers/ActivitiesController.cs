﻿namespace RushHour.Web.Controllers
{
    using Data;
    using Generic;
    using Models.Binding.Activity;
    using Models.Entities;
    using Models.View.Activity;
    using Services.Generic;

    public class ActivitiesController : CrudController<CrudService<UnitOfWork, Activity, ActivityIndexViewModel, ActivityBindingModel, ActivityDetailsViewModel, ActivityFilterBindingModel>, ActivityIndexViewModel, ActivityBindingModel, ActivityDetailsViewModel, ActivityFilterBindingModel>
    {
    }
}