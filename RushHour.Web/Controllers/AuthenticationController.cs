﻿namespace RushHour.Web.Controllers
{
    using System.Web.Mvc;
    using Generic;
    using Models.Binding.User;
    using Services.Tools.Messages;

    public class AuthenticationController : BaseController
    {
        [HttpGet]
        public ActionResult Login()
        {
            if (AuthenticationManager.LoggedUser == null)
                return View();


            AddError(GlobalMessages.AlreadyLoggedIn());
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(LoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (AuthenticationManager.LoggedUser != null)
            {
                AddError(GlobalMessages.AlreadyLoggedIn());
                return RedirectToAction("Index", "Home");
            }

            AuthenticationManager.Authenticate(model);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {

            if (AuthenticationManager.LoggedUser == null)
                return View();


            AddError(GlobalMessages.AlreadyLoggedIn());
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (AuthenticationManager.LoggedUser != null)
            {
                AddError(GlobalMessages.ShouldLogoutBeforeRegister());
                return RedirectToAction("Index", "Home");
            }

            AuthenticationManager.Register(model);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            if (AuthenticationManager.LoggedUser == null)
            {
                AddError(GlobalMessages.NotLoggedIn());
                return RedirectToAction("Index", "Home");
            }
            AuthenticationManager.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}