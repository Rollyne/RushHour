﻿namespace RushHour.Web.Controllers.Generic
{

    public abstract class ServiceController<TService> : BaseController
        where TService : new()
    {
        protected TService Service;
        protected ServiceController(TService service)
        {
            Service = service;
        }

        protected ServiceController()
        {
            Service = new TService();
        }

        
    }
}