﻿namespace RushHour.Web.Controllers.Generic
{
    using System.Web.Mvc;
    using Models.Binding;
    using Services.Generic;

    public abstract class CrudController<TService,TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel> : ServiceController<TService> 

        where TService : ICrudService<TIndexViewModel,TBindingModel,TDetailsViewModel, TFilterBindingModel>, new()
        where TFilterBindingModel : IPageFilter
    {
        // GET: List
        public virtual ActionResult Index(TFilterBindingModel model)
        {
            var execution = Service.GetAllFiltered(model);
            if (execution.Succeeded)
            {
                AddMessage(execution.Message);
                return View(execution.Result.Model);
            }

            AddError(execution.Message);
            return RedirectToAction("Index", "Home");
        }

        // GET: Crud/Details/5
        public ActionResult Details(int id)
        {
            var execution = Service.GetDetails(id);
            if (execution.Succeeded)
            {
                AddMessage(execution.Message);
                return View(execution.Result);
            }

            AddError(execution.Message);
            return RedirectToAction("Index", "Home");
        }

        // GET: Crud/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Crud/Create
        [HttpPost]
        public ActionResult Create(TBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var execution = Service.AddOrUpdate(model);
            if (execution.Succeeded)
            {
                AddMessage(execution.Message);
                return RedirectToAction("Index");
            }
            
            AddError(execution.Message);
            return View();
        }

        // GET: Crud/Edit/5
        public ActionResult Edit(int id)
        {
            var execution = Service.GetForModification(id);
            if (execution.Succeeded)
            {
                AddMessage(execution.Message);
                return View(execution.Result);
            }

            AddError(execution.Message);
            return RedirectToAction("Index", "Home");
        }

        // POST: Crud/Edit/5
        [HttpPost]
        public ActionResult Edit(TBindingModel model)
        {
            return Create(model);
        }

        // GET: Crud/Delete/5
        public ActionResult ConfirmDelete(int id)
        {
            return Edit(id);
        }

        // POST: Crud/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var execution = Service.Delete(id);
            if (execution.Succeeded)
            {
                AddMessage(execution.Message);
                return RedirectToAction("Index");
            }

            AddError(execution.Message);
            return View();
        }
    }
}
