﻿namespace RushHour.Web.Controllers
{
    using System.Web.Mvc;
    using Generic;

    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
    }
}
