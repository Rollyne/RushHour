﻿namespace RushHour.Web
{
    using System.Web;
    using Models.Binding.User;
    using Models.Entities;
    using Services;

    public static class AuthenticationManager
    {
        public static User LoggedUser
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                    HttpContext.Current.Session["LoggedUser"] = new AuthenticationService();

                var authenticationService = HttpContext.Current?.Session["LoggedUser"] as AuthenticationService;
                return authenticationService?.LoggedUser;
            }
        }

        public static void Authenticate(LoginBindingModel model)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                HttpContext.Current.Session["LoggedUser"] = new AuthenticationService();

            var authenticationService = HttpContext.Current?.Session["LoggedUser"] as AuthenticationService;
            authenticationService?.AuthenticateUser(model);
        }

        public static void Register(RegisterBindingModel model)
        {
            var authenticationService = new AuthenticationService();
            authenticationService.Register(model);

            Authenticate(new LoginBindingModel()
            {
                Email = model.Email,
                Password = model.Password
            });
        }

        public static void Logout()
        {
            HttpContext.Current.Session["LoggedUser"] = null;
        }
    }
}