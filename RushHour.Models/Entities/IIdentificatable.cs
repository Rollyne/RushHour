﻿namespace RushHour.Models.Entities
{
    public interface IIdentificatable
    {
        int Id { get; set; }
    }
}
