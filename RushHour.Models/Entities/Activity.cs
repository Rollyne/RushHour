﻿namespace RushHour.Models.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Activity : IIdentificatable
    {
        private ICollection<Appointment> _appointments;

        public Activity()
        {
            _appointments = new HashSet<Appointment>();
        }

        [Key]
        public int Id { get; set; }

        [MinLength(3)]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        
        public double Duration { get; set; }

        public decimal Price { get; set; }

        public virtual ICollection<Appointment> Appointments
        {
            get { return _appointments; }
            set { _appointments = value; }
        }
    }
}
