﻿namespace RushHour.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Appointment : IIdentificatable
    {
        private ICollection<Activity> _activities;

        public Appointment()
        {
            _activities = new HashSet<Activity>();
        }

        [Key]
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Activity> Activities
        {
            get { return _activities; }
            set { _activities = value; }
        }
    }
}
