﻿namespace RushHour.Models.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using DataAnnotationsExtensions;

    public class User : IIdentificatable
    {
        private ICollection<Appointment> _appointments;

        public User()
        {
            _appointments = new HashSet<Appointment>();
        }

        [Key]
        public int Id { get; set; }

        [Email]
        [Required]
        [Index("EmailIndex", IsUnique = true)]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(20)]
        public string Password { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }

        [MinLength(3)]
        [MaxLength(20)]
        public string Phone { get; set; }

        public virtual ICollection<Appointment> Appointments
        {
            get { return _appointments; }
            set { _appointments = value; }
        }
    }
}
