﻿namespace RushHour.Models.Binding.Appointment
{
    public class AppointmentFilterBindingModel : IPageFilter
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }
}
