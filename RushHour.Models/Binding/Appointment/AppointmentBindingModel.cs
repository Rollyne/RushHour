﻿namespace RushHour.Models.Binding.Appointment
{
    using System;
    public class AppointmentBindingModel
    {
        public int? Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
