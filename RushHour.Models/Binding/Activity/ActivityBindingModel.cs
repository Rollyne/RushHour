﻿namespace RushHour.Models.Binding.Activity
{
    using System.ComponentModel.DataAnnotations;

    public class ActivityBindingModel
    {
        public int? Id { get; set; }

        [MinLength(3)]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        public double Duration { get; set; }

        public decimal Price { get; set; }
    }
}
