﻿namespace RushHour.Models.Binding.Activity
{
    public class ActivityFilterBindingModel : IPageFilter
    {
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; }
    }
}
