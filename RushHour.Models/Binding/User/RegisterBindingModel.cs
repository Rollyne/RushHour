﻿namespace RushHour.Models.Binding.User
{
    using System.ComponentModel.DataAnnotations;
    using DataAnnotationsExtensions;

    public class RegisterBindingModel
    {
        [Email]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        public string Password { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [MinLength(3)]
        public string Phone { get; set; }
    }
}
