﻿namespace RushHour.Models.Binding.User
{
    public class UserFilterBindingModel : IPageFilter
    {
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; }
    }
}
