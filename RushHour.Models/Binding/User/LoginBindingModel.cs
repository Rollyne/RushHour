﻿namespace RushHour.Models.Binding.User
{
    using System.ComponentModel.DataAnnotations;
    using DataAnnotationsExtensions;

    public class LoginBindingModel
    {
        [Required]
        [Email]
        public string Email { get; set; }

        [Required]
        [MinLength(3)]
        public string Password { get; set; }
    }
}
