﻿namespace RushHour.Models.Binding
{
    public interface IPageFilter
    {
        int Page { get; set; }
        int ItemsPerPage { get; set; }
    }
}
