﻿namespace RushHour.Models.Mapper
{
    using AutoMapper;
    using Binding;
    using Binding.User;
    using Entities;
    using View.User;

    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserIndexViewModel>();
            CreateMap<User, UserDetailsViewModel>();
            CreateMap<User, UserBindingModel>();
            CreateMap<UserBindingModel, User>();
            CreateMap<RegisterBindingModel, User>();
        }
    }
}
