﻿namespace RushHour.Models.Mapper
{
    using AutoMapper;
    using Binding.Appointment;
    using Entities;
    using View.Appointment;

    public class AppointmentProfile : Profile
    {
        public AppointmentProfile()
        {
            CreateMap<Appointment, AppointmentIndexViewModel>();
            CreateMap<Appointment, AppointmentDetailsViewModel>()
                .ForMember(dest => dest.TimeLeft, opt => opt.Ignore());

            CreateMap<Appointment, AppointmentBindingModel>();
            CreateMap<AppointmentBindingModel, Appointment>();
        }
    }
}
