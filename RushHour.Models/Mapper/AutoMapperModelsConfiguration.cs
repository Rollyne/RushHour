﻿namespace RushHour.Models.Mapper
{
    public static class AutoMapperModelsConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(cfg =>
                {
                    cfg.AddProfile<UserProfile>();
                    cfg.AddProfile<ActivityProfile>();
                    cfg.AddProfile<AppointmentProfile>();
                }
            );
        }
    }
}
