﻿namespace RushHour.Models.Mapper
{
    using AutoMapper;
    using Binding.Activity;
    using Entities;
    using View.Activity;

    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            CreateMap<Activity, ActivityIndexViewModel>();
            CreateMap<Activity, ActivityDetailsViewModel>()
                .ForMember(dest => dest.AppointmentsCount, opt => opt.MapFrom(src => src.Appointments.Count));
            CreateMap<Activity, ActivityBindingModel>();
            CreateMap<ActivityBindingModel, Activity>();
        }
    }
}
