﻿namespace RushHour.Models.View.Activity
{
    public class ActivityIndexViewModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public double Duration { get; set; }

        public decimal Price { get; set; }
    }
}
