﻿namespace RushHour.Models.View.Activity
{
    using System.ComponentModel;

    public class ActivityDetailsViewModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public double Duration { get; set; }

        public decimal Price { get; set; }

        [DisplayName("Count of appointments")]
        public int AppointmentsCount { get; set; }
    }
}
