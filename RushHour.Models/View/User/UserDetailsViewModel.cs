﻿namespace RushHour.Models.View.User
{
    public class UserDetailsViewModel
    {
        public int Id { get; set; }
        
        public string Email { get; set; }
        
        public string Password { get; set; }
        
        public string Name { get; set; }
        
        public string Phone { get; set; }
    }
}
