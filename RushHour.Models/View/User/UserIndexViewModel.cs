﻿namespace RushHour.Models.View.User
{
    public class UserIndexViewModel
    {
        public int Id { get; set; }
        
        public string Email { get; set; }

        public string Name { get; set; }
    }
}
