﻿namespace RushHour.Models.View.Appointment
{
    using System;
    public class AppointmentIndexViewModel
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
