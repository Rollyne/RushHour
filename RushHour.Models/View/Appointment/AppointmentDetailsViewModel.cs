﻿namespace RushHour.Models.View.Appointment
{
    using System;
    using System.ComponentModel;

    public class AppointmentDetailsViewModel
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        [DisplayName("Time left")]
        public TimeSpan TimeLeft => End.Subtract(Start);
    }
}
