﻿namespace RushHour.Services.Tools
{
    using System.Net;

    public class ExecutionResult : IExecutionResult
    {
        public bool Succeeded { get; set; }

        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
