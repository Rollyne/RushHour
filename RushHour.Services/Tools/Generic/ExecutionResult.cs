﻿namespace RushHour.Services.Tools.Generic
{
    public class ExecutionResult<TModel> : ExecutionResult, IExecutionResult<TModel>
    {
        public TModel Result { get; set; }
    }
}
