﻿namespace RushHour.Services.Tools.Generic
{
    public interface IExecutionResult<TModel> : IExecutionResult
    {
        TModel Result { get; set; }
    }
}