﻿using System.Net;

namespace RushHour.Services.Tools
{
    public interface IExecutionResult
    {
        string Message { get; set; }
        HttpStatusCode StatusCode { get; set; }
        bool Succeeded { get; set; }
    }
}