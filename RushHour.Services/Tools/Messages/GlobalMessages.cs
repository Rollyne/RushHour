﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Services.Tools.Messages
{
    public static class GlobalMessages
    {
        public static string InternalError()
            => "There was an internal server error. Please report the steps you took to this error.";

        public static string UserFound() => "Your profile was found successfully.";

        public static string SuccessfullyRegistered() => "You were successfully registered.";

        public static string AlreadyLoggedIn() => "You are already logged in.";

        public static string ShouldLogoutBeforeRegister() => "You should logout before you register a new user.";

        public static string NotLoggedIn() => "You are not logged in.";
    }
}
