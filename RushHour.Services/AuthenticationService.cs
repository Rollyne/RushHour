﻿namespace RushHour.Services
{
    using Data;
    using Generic;
    using Models.Binding.User;
    using Models.Entities;

    public class AuthenticationService : Service<UnitOfWork>
    {
        public User LoggedUser { get; private set; }

        public void AuthenticateUser(LoginBindingModel model)
        {
            var userRepo = UnitOfWork.GetRepository<User>();
            LoggedUser = userRepo.FirstOrDefault(where: u => u.Email == model.Email && u.Password == model.Password);
        }

        public void Register(RegisterBindingModel model)
        {
            var userRepo = UnitOfWork.GetRepository<User>();
            var item = AutoMapper.Mapper.Map<User>(model);

            userRepo.AddOrUpdate(item);
            UnitOfWork.Save();

        }
    }
}
