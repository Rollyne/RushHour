﻿using RushHour.Data.Repositories.Tools;
using RushHour.Models.Binding;
using RushHour.Models.Entities;
using RushHour.Services.Tools;
using RushHour.Services.Tools.Generic;

namespace RushHour.Services.Generic
{
    public interface ICrudService<TIndexViewModel, TBindingModel, TDetailsViewModel, TFilterBindingModel>
        where TFilterBindingModel : IPageFilter
    {
        IExecutionResult AddOrUpdate(TBindingModel item);
        IExecutionResult Delete(int id);
        IExecutionResult<IPager<TIndexViewModel>> GetAllFiltered(TFilterBindingModel filter);
        IExecutionResult<TDetailsViewModel> GetDetails(int id);
        IExecutionResult<TBindingModel> GetForModification(int id);
    }
}