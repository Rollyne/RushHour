﻿namespace RushHour.Services.Generic
{
    using Data;

    public abstract class Service<TUnitOfWork>
        where TUnitOfWork : IUnitOfWork, new()
    {
        protected TUnitOfWork UnitOfWork;
        protected Service(TUnitOfWork unit)
        {
            UnitOfWork = unit;
        }

        protected Service()
        {
            UnitOfWork = new TUnitOfWork();
        }
    }

}
